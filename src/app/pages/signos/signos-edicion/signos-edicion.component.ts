import { PacienteService } from './../../../_service/paciente.service';
import { SignosService } from './../../../_service/signos.service';
import { Signos } from './../../../_model/signos';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Paciente } from './../../../_model/paciente';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  pacientes: Paciente[] = [];
  idPacienteSeleccionado: number;
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  temperatura: number;
  pulso: number;
  ritmoRespiratorio: number;

  id: number;
  signos: Signos;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private pacienteService: PacienteService, private signosService: SignosService, private route: ActivatedRoute, private router: Router) {
    this.signos = new Signos();
    
  }

  ngOnInit() {
    this.listarPacientes();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  listarPacientes() {
    this.pacienteService.listarPacientes().subscribe(data => {
      this.pacientes = data;
    });
  }

  private initForm() {
    if (this.edicion) {
      this.signosService.listarSignosPorId(this.id).subscribe(data => {
        this.id = data.idSignos;
        this.idPacienteSeleccionado = data.paciente.idPaciente;
        this.fechaSeleccionada = new Date(data.fecha);
        this.temperatura = data.temperatura;
        this.pulso = data.pulso;
        this.ritmoRespiratorio = data.ritmoRespiratorio;
      });
    }
  }

  aceptar() {
    let paciente = new Paciente();
    paciente.idPaciente = this.idPacienteSeleccionado;
    var tzoffset = (this.fechaSeleccionada).getTimezoneOffset() * 60000; //offset in milliseconds
    var localISOTime = (new Date(Date.now() - tzoffset)).toISOString();

    this.signos.idSignos= this.id;
    this.signos.paciente = paciente;
    this.signos.fecha = (this.fechaSeleccionada).toISOString();
    this.signos.temperatura = this.temperatura;
    this.signos.pulso = this.pulso;
    this.signos.ritmoRespiratorio = this.ritmoRespiratorio;
  
    if (this.signos != null && this.signos.idSignos > 0) {      
      this.signosService.modificar(this.signos).subscribe(data => {
        this.signosService.listarSignos().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensaje.next("Se modifico");
        });
      });
    } else {
      this.signosService.registrar(this.signos).subscribe(data => {
        this.signosService.listarSignos().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensaje.next("Se registro");
        });
      });
    }

    this.signosService.listarSignos().subscribe(data => {
      this.signosService.signosCambio.next(data);
    });

    this.router.navigate(['signos']);
  }

  estadoBotonRegistrar() {
    return (this.idPacienteSeleccionado === 0);
  }

}
