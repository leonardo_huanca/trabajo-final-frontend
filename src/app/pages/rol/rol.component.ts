import { RolService } from './../../_service/rol.service';
import { DialogoRolComponent } from './dialogo-rol/dialogo-rol.component';
import { Rol } from './../../_model/rol';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  roles: Rol[] = [];
  displayedColumns = ['idRol', 'nombre', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<Rol>;
  mensaje: string;
  cantidad : number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private rolService: RolService, public dialog: MatDialog, public snackBar: MatSnackBar) {
    
  }

  ngOnInit() {
    this.rolService.rolCambio.subscribe(data => {
      this.roles = data;
      this.dataSource = new MatTableDataSource(this.roles);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.rolService.mensaje.subscribe(data => {
      console.log(data);
      this.snackBar.open(data, null, { duration: 2000 });
    });

    this.rolService.listarRolesPageable(0, 10).subscribe(data => {
      let roles = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(roles);
      this.dataSource.sort = this.sort;
    });    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(rol: Rol): void {
    let ro = rol != null ? rol : new Rol();
    let dialogRef = this.dialog.open(DialogoRolComponent, {
      width: '250px',   
      disableClose: true,   
      data: ro      
    });
  }

  mostrarMas(e: any){
    console.log(e);
    this.rolService.listarRolesPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let roles = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource= new MatTableDataSource(roles);      
      this.dataSource.sort = this.sort;      
    });
  }

}
