import { RolService } from '../../../_service/rol.service';
import { Rol } from '../../../_model/rol';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogo-rol',
  templateUrl: './dialogo-rol.component.html',
  styleUrls: ['./dialogo-rol.component.css']
})
export class DialogoRolComponent implements OnInit {

  rol: Rol;

  constructor(public dialogRef: MatDialogRef<DialogoRolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Rol,
    private rolService: RolService) {

  }

  ngOnInit() {
    this.rol = new Rol();
    this.rol.idRol = this.data.idRol;
    this.rol.nombre = this.data.nombre;
    this.rol.descripcion = this.data.descripcion;    
  }

  operar() {

    if (this.rol != null && this.rol.idRol > 0) {
      this.rolService.modificar(this.rol).subscribe(data => {
        this.rolService.listarRoles().subscribe(roles => {
          this.rolService.rolCambio.next(roles);
          this.rolService.mensaje.next("Se modifico");
        });
      });
    } else {
      this.rolService.registrar(this.rol).subscribe(data => {        
          this.rolService.listarRoles().subscribe(roles => {
            this.rolService.rolCambio.next(roles);
            this.rolService.mensaje.next("Se registro");
          });        
      });
    }
    this.dialogRef.close();
  }

  cancelar() {
    this.dialogRef.close();
  }

}
