import { TOKEN_NAME } from './../../_shared/var.constant';
import { Component, OnInit } from '@angular/core';
import * as decode from 'jwt-decode';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario: string;
  roles: string;
  access_token: any;

  constructor(private bottomSheetRef: MatBottomSheetRef<PerfilComponent>) {}

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }


  ngOnInit() {
    this.access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    const decodedToken = decode(this.access_token);
    this.usuario = decodedToken.user_name;
    this.roles = decodedToken.authorities.join(', ');
  }

}
