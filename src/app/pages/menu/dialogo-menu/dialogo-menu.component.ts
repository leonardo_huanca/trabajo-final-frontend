import { MenuService } from '../../../_service/menu.service';
import { Menu } from '../../../_model/menu';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogo-menu',
  templateUrl: './dialogo-menu.component.html',
  styleUrls: ['./dialogo-menu.component.css']
})
export class DialogoMenuComponent implements OnInit {

  menu: Menu;

  constructor(public dialogRef: MatDialogRef<DialogoMenuComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Menu,
    private menuService: MenuService) {

  }

  ngOnInit() {
    this.menu = new Menu();
    this.menu.idMenu = this.data.idMenu;
    this.menu.nombre = this.data.nombre;
    this.menu.icono = this.data.icono;
    this.menu.url = this.data.url;
  }

  operar() {

    if (this.menu != null && this.menu.idMenu > 0) {
      this.menuService.modificar(this.menu).subscribe(data => {
        this.menuService.listar().subscribe(roles => {
          this.menuService.menuCambio.next(roles);
          this.menuService.mensaje.next("Se modifico");
        });
      });
    } else {
      this.menuService.registrar(this.menu).subscribe(data => {        
          this.menuService.listar().subscribe(roles => {
            this.menuService.menuCambio.next(roles);
            this.menuService.mensaje.next("Se registro");
          });        
      });
    }
    this.dialogRef.close();
  }

  cancelar() {
    this.dialogRef.close();
  }

}
