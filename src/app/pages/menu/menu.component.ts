import { MenuService } from './../../_service/menu.service';
import { DialogoMenuComponent } from './dialogo-menu/dialogo-menu.component';
import { Menu } from './../../_model/menu';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menus: Menu[] = [];
  displayedColumns = ['idMenu', 'nombre', 'icono', 'url', 'acciones'];
  dataSource: MatTableDataSource<Menu>;
  mensaje: string;
  cantidad : number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private menuService: MenuService, public dialog: MatDialog, public snackBar: MatSnackBar) {
    
  }

  ngOnInit() {
    this.menuService.menuCambio.subscribe(data => {
      this.menus = data;
      this.dataSource = new MatTableDataSource(this.menus);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.menuService.mensaje.subscribe(data => {
      console.log(data);
      this.snackBar.open(data, null, { duration: 2000 });
    });

    this.menuService.listarMenusPageable(0, 10).subscribe(data => {
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(menus);
      this.dataSource.sort = this.sort;
    });    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(menu: Menu): void {
    let me = menu != null ? menu : new Menu();
    let dialogRef = this.dialog.open(DialogoMenuComponent, {
      width: '250px',   
      disableClose: true,   
      data: me      
    });
  }

  mostrarMas(e: any){
    console.log(e);
    this.menuService.listarMenusPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource= new MatTableDataSource(menus);      
      this.dataSource.sort = this.sort;      
    });
  }

}
