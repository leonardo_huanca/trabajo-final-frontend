import { Paciente } from './paciente';
export class Signos {
    public idSignos: number;
    public paciente: Paciente ;
    public fecha: string;
    public temperatura: number;
    public pulso: number;
    public ritmoRespiratorio: number;
}